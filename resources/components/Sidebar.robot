***Settings***
Documentation       Representação do menu lateral de navegação na área logada

##Todas as variáveis que representam elementos de página/componentes estão em maiúscula
##As demais são snake case (minúsculas com underscore)
***Variables***
${NAV_CUSTOMERS}            css:a[href$=customers]