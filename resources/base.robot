***Settings***

Library         SeleniumLibrary
Library         libs/db.py

Resource        kws.robot
Resource        hooks.robot

Resource        pages/LoginPage.robot
# Resource        pages/ContractsPage.robot
Resource        pages/CustomersPage.robot
# Resource        pages/EquiposPage.robot

Resource        components/Sidebar.robot
Resource        components/Toaster.robot


***Variables***

# #Simples
# ${name}     Fernanda Rocha

# #Lista //como se fosse um array
# @{carros}       Civic       Lancer      Accord      Chevette

# #Dicionário
# &{cliente}      nome=Bon Jovi       cpf=0000000001406       email=bon@teste.com     telefone=119999999999

${base_url}           http://zepalheta-web:3000/ 
${admin_user}         admin@zepalheta.com.br
${admin_pass}         pwd123