***Settings***
Documentation       Cadastro de clientes
Resource        ../resources/base.robot

Suite Setup          Login Session
Suite Teardown       Finish Session

***Test Cases***
Novo cliente
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...     Bon Jovi            00000001406         Rua dos Bugs, 1000          119999999999
    Quando faço a inclusão desse cliente    
    Então devo ver a notificação:       Cliente cadastrado com sucesso!

Cliente Duplicado
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...     Adrian Smith        00000001410         Rua dos Bugs, 100           119999999998
    Mas esse CPF já existe no sistema 
    Quando faço a inclusão desse cliente    
    Então devo ver a notificação de erro:       Este CPF já existe no sistema!

Campos Obrigatórios
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...     ${EMPTY}    ${EMPTY}    ${EMPTY}    ${EMPTY}
    Quando faço a inclusão desse cliente     
    Então devo ver mensagens informando que os campos do cadastro de clientes são obrigatórios

Nome é obrigatório
    [Tags]          required
    [Template]      Validação de Campos
    ${EMPTY}        12345678900     Rua Floresta, 100       48999999999         Nome é obrigatório

CPF é obrigatório 
    [Tags]          required
    [Template]      Validação de Campos
    Maria João      ${EMPTY}        Rua Floresta, 100       48999999999         CPF é obrigatório

Endereço é obrigatório 
    [Tags]          required
    [Template]      Validação de Campos
    Maria João      12345678900     ${EMPTY}                48999999999         Endereço é obrigatório

Telefone é obrigatório 
    [Tags]          required
    [Template]      Validação de Campos
    Maria João      12345678900     Rua Floresta, 100       ${EMPTY}            Telefone é obrigatório
     
Telefone incorreto
    [Template]      Validação de Campos
    Maria João      12345678900     Rua Floresta, 100       4899999999          Telefone inválido

***Keywords***
Validação de Campos
    [Arguments]         ${name}         ${cpf}          ${address}          ${phone_number}     ${output_message}

    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...     ${name}         ${cpf}          ${address}          ${phone_number}
    Quando faço a inclusão desse cliente    
    Então devo ver mensagem de alerta       ${output_message}


