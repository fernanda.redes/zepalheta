## OUTRA FORMA DE FAZER TEMPLATE NO PRÓPRIO TESTCASE 
## UM JEITO DIFERENTE MAS QUE FAZ A MESMA COISA QUE O CADASTRO_CLIENTES > validação de campos

# ***Settings***
# Documentation       Tentativa cadastro de cliente 
# Resource        ../resources/base.robot

# Test Setup          Login Session
# Test Teardown       Finish Session
# Test Template       Tentativa de Cadastro 

# ***Keywords***
# Tentativa de Cadastro 
#     [Arguments]         ${name}         ${cpf}          ${address}          ${phone_number}     ${output_message}

#     Dado que acesso o formulário de cadastro de clientes
#     Quando faço a inclusão desse cliente:  
#     ...     ${name}         ${cpf}          ${address}          ${phone_number}
#     Então devo ver mensagem de alerta       ${output_message}


# ***Test Cases***
# Nome é obrigatório          ${EMPTY}        12345678900     Rua Floresta, 100       48999999999         Nome é obrigatório
# CPF é obrigatório           Maria João      ${EMPTY}        Rua Floresta, 100       48999999999         CPF é obrigatório
# Endereço é obrigatório      Maria João      12345678900     ${EMPTY}                48999999999         Endereço é obrigatório
# Telefone é obrigatório      Maria João      12345678900     Rua Floresta, 100       ${EMPTY}            Telefone é obrigatório
